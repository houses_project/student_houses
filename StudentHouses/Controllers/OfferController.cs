﻿using StudentHouses.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace StudentHouses.Controllers
{
    public class OfferController : Controller
    {
        HouseContext db = new HouseContext();
        ApplicationDbContext UserDb = new ApplicationDbContext();
        // GET: House
        public ActionResult List()
        {
            IEnumerable<House> houses = db.Houses;
            ViewBag.Houses = houses;
            return View();
        }

        [Authorize]
        public ActionResult AddHouse()
        {
            return View();
        }

        public ActionResult Show(int houseId)
        {
            House house = db.Houses.FirstOrDefault(x => x.ID == houseId);
            ViewBag.House = house;
            return View();
        }
        [HttpPost]
        public ActionResult AddHouse(House house, IEnumerable<HttpPostedFileBase> images)
        {
            // Получение текущего пользователя по Id
            var currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = UserDb.Users.FirstOrDefault(x => x.Id == currentUserId);
            house.Owner = User.Identity.Name;
            house.PhoneNumber = currentUser.Number;
            StringBuilder sb = new StringBuilder();
            foreach (var file in images)
            {
                if (file != null)
                {
                    // получаем имя файла
                    string fileName = System.IO.Path.GetFileName(file.FileName);
                    sb.Append(fileName);
                    sb.Append("\r\n");
                    // сохраняем файл в папку Files в проекте
                    file.SaveAs(Server.MapPath("~/Image/" + fileName));                    
                }
            }
            house.Images = sb.ToString();
            db.Houses.Add(house);
            //сохраняем в бд все изменения
            db.SaveChanges();
            ViewBag.Message = "Успешно!";
            return View();
        }
    }
}