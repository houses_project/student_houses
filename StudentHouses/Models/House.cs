﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentHouses.Models
{
    public class House
    {
        public int ID { get; set; }
        public string Address { get; set; }
        // собственник
        public string Owner { get; set; }
        // цена
        public string PhoneNumber { get; set; }
        public int Price { get; set; }
        // тип жилья
        public string Type { get; set; }
        public string Images { get; set; }
    }
}