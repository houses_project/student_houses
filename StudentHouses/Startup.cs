﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StudentHouses.Startup))]
namespace StudentHouses
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
